package io.schiar.mrmine

/**
 * Created by Giovani on 29/08/2017.
 */

import io.schiar.mrmine.model.*

class SequencePlanner {

    fun newSequenceListFromBoard(currentSequences: List<Sequence>?, newSequence : Sequence) : MutableList<Sequence> {
        val sequences = currentSequences ?: return mutableListOf()

        val newSequences = mutableListOf<Sequence>()
        if (sequences.isEmpty()) {
            newSequences.add(newSequence)
        } else {
            newSequences.addAll(sequences)
            newSequences.add(newSequence)

            val newMergedSequences = mutableListOf<Sequence>()

            newSequences.filter { it != newSequence }.forEach {
                val mergedSequence = merge(it, newSequence, newSequences)
                if(mergedSequence != null) {
                    newMergedSequences.add(mergedSequence)
                }

                if (it.direction != Directions.UNIQUE) {
                    it.breakIntoUniqueSequences().forEach {
                        val mergedSequence = merge(it, newSequence, newSequences)
                        if (mergedSequence != null) {
                            newMergedSequences.add(mergedSequence)
                        }
                    }
                }
            }

            for (sequence in newMergedSequences) {
                tryMergeSequenceWithList(sequence, newMergedSequences.filter { it != sequence }, newSequences)
            }
        }

        return newSequences
    }

    fun tryMergeSequenceWithList(seq1: Sequence, sequences: List<Sequence>, newSequences: MutableList<Sequence>) {
        var currentSeq = seq1
        var merged: Boolean
        do {
            merged = false
            for (otherSequence in sequences) {
                val mergedSeq = merge(currentSeq, otherSequence, newSequences)
                if (mergedSeq != null) {
                    merged = true
                    currentSeq = mergedSeq
                }
            }
        } while (merged)
    }

    fun merge(seq1: Sequence, seq2: Sequence, sequences: MutableList<Sequence>): Sequence? {
        if (seq2.direction == Directions.UNIQUE) {
            return mergeUniqueSequence(seq1, seq2, sequences)
        }

        if (!seq1.isCompatibleTo(seq2)) {
            return null
        }

        var mergedSequence: Sequence? = null
        var currentSeq1 = seq1
        val uniqueSeqs2 = seq2.breakIntoUniqueSequences().filter { !currentSeq1.contains(it) }.toMutableList()
        var merged: Boolean
        do {
            merged = false
            for (seq in uniqueSeqs2) {
                val mergedSeq = mergeUniqueSequence(currentSeq1, seq, sequences)
                if (mergedSeq != null) {
                    merged = true
                    currentSeq1 = mergedSeq
                    mergedSequence = mergedSeq
                }
            }
        } while (merged)

        return mergedSequence
    }

    fun mergeUniqueSequence(seq1: Sequence, uniqueSeq2: Sequence, sequences: MutableList<Sequence>): Sequence? {
        var newSequence : Sequence? = null
        if(seq1.direction == Directions.UNIQUE) {
            if(seq1.point.neighbor(uniqueSeq2.point)) {
                newSequence = Sequence(seq1.point, uniqueSeq2.point.directionTo(seq1.point), seq1.quantity+uniqueSeq2.quantity)
            }
        } else if(seq1.nextSequencePoint() == uniqueSeq2.point) {
            newSequence = Sequence(seq1.point, seq1.direction, seq1.quantity+uniqueSeq2.quantity)
        } else if(seq1.previousSequencePoint() == uniqueSeq2.point) {
            newSequence = Sequence(uniqueSeq2.point, seq1.direction, seq1.quantity+uniqueSeq2.quantity)
        }

        if (newSequence != null) {
            sequences.remove(seq1)
            sequences.remove(uniqueSeq2)
            for(sequence in sequences) {
                if(newSequence in sequence)
                    return null
            }
            sequences.add(newSequence)
        }
        return newSequence
    }
}