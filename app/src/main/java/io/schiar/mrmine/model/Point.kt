package io.schiar.mrmine.model

import io.schiar.mrmine.R
import io.schiar.mrmine.ResourceProvider

/**
 * Created by Giovani on 21/08/2017.
 */
data class Point(val i: Int, val j: Int) {
    override operator fun equals(other: Any?): Boolean = (i == (other as Point).i && j == other.j)

    override fun toString(): String = "Point($i, $j)"

    override fun hashCode(): Int {
        var result = i
        result = 31 * result + j
        return result
    }
}

fun Point.outOfGrid(): Boolean {
    return i < 0 || i > ResourceProvider.int(R.integer.sizeOfGrid)-1 || j < 0 || j > ResourceProvider.int(R.integer.sizeOfGrid)-1
}

fun Point.neighbors() : MutableList<Point> {
    val neighbors = mutableListOf<Point>()
    val minI = maxOf(0, i-1)
    val minJ = maxOf(0, j-1)
    val maxI = minOf(ResourceProvider.int(R.integer.sizeOfGrid)-1, i+1)
    val maxJ = minOf(ResourceProvider.int(R.integer.sizeOfGrid)-1, j+1)
    for (i in minI..maxI) {
        for (j in minJ..maxJ) {
            val neighbor = Point(i, j)
            if (neighbor != this) {
                neighbors.add(neighbor)
            }
        }
    }
    return neighbors
}

fun Point.neighbor(other: Point) : Boolean {
    return (other.i in i-1..i+1) && (other.j in j-1..j+1)
}

fun Point.directionTo(other: Point) : Directions {
    if(this == other.goN(1))
        return Directions.N
    else if (this == other.goS(1))
        return Directions.S
    else if (this == other.goE(1))
        return Directions.E
    else if (this == other.goW(1))
        return Directions.W
    else if (this == other.goNE(1))
        return Directions.NE
    else if (this == other.goNW(1))
        return Directions.NW
    else if (this == other.goSE(1))
        return Directions.SE
    else if (this == other.goSW(1))
        return Directions.SW
    else return Directions.UNIQUE
}

fun Point.goN(steps: Int) : Point {
    return Point(i-steps, j)
}

fun Point.goS(steps: Int) : Point {
    return Point(i+steps, j)
}

fun Point.goE(steps: Int) : Point {
    return Point(i, j+steps)
}

fun Point.goW(steps: Int) : Point {
    return Point(i, j-steps)
}

fun Point.goNE(steps: Int) : Point {
    return Point(i-steps, j+steps)
}

fun Point.goNW(steps: Int) : Point {
    return Point(i-steps, j-steps)
}

fun Point.goSE(steps: Int) : Point {
    return Point(i+steps, j+steps)
}

fun Point.goSW(steps: Int) : Point {
    return Point(i+steps, j-steps)
}
