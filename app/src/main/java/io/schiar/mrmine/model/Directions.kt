package io.schiar.mrmine.model

/**
 * Created by Giovani on 21/08/2017.
 */
enum class Directions {
    N, S, W, E, NE, NW, SW, SE, UNIQUE
}