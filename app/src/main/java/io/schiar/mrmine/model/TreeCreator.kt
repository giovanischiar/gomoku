package io.schiar.mrmine.model

import io.schiar.mrmine.R
import io.schiar.mrmine.ResourceProvider

/**
 * Created by Giovani on 03/09/17.
 */
class TreeCreator(val initial: Point, val maxPiece: Char, val minPiece: Char) {
    enum class Direction {
        RIGHT, UP, LEFT, DOWN;

        fun next(): Direction = when(this) {
            RIGHT -> UP
            UP -> LEFT
            LEFT -> DOWN
            DOWN -> RIGHT
        }
    }

    fun createTree(node: Node, level: Int, currentMoves: List<Move>) {
        val allMoves = currentMoves + node.moves
        val n = ResourceProvider.int(R.integer.sizeOfGrid)
        val size = n * n - allMoves.size
        var p = 1
        var i = initial.i
        var j = initial.j
        var d = Direction.RIGHT

        if(!initial.outOfGrid() && allMoves.none { it.point == initial }) {
            createNode(node, level, allMoves, initial)
        }

        while(node.children.size < size) {
            for(l in 0..1) {
                for(k in 1..p) {
                    when(d) {
                        Direction.RIGHT -> j+=1
                        Direction.UP -> i-=1
                        Direction.LEFT -> j-=1
                        Direction.DOWN -> i+=1
                    }
                    val newPoint = Point(i, j)
                    if(!newPoint.outOfGrid() && allMoves.none { it.point == newPoint }){
                        createNode(node, level, allMoves, newPoint)
                        if(node.children.size >= size)
                            break
                    }
                }
                if(node.children.size >= size)
                    break
                d = d.next()
            }
            ++p
        }
    }

    private fun createNode(node: Node, level: Int, moves: List<Move>, newPoint: Point) {
        val newMoves = mutableListOf<Move>()
        newMoves.addAll(node.moves)
        val newMove = Move(newPoint, if (level % 2 == 0) maxPiece else minPiece)
        newMoves.add(newMove)
        val child = Node(mutableListOf<Node>(), newMoves)
        node.children.add(child)
        val newLevel = level + 1
        if(newLevel <= 1)
            createTree(child, newLevel, moves)
    }
}