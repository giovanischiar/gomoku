package io.schiar.mrmine.model

/**
 * Created by Giovani on 30/08/2017.
 */
data class Move(val point: Point, val piece: Char)