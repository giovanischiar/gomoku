package io.schiar.mrmine.model

import io.schiar.mrmine.SequencePlanner

/**
 * Created by Giovani on 20/08/2017.
 */
open class Player(val name: String, var piece: Char) {
    var sequences: MutableList<Sequence> = ArrayList()

    fun addTurn(i: Int, j: Int) {
        sequences = SequencePlanner().newSequenceListFromBoard(sequences, Sequence(Point(i, j), Directions.UNIQUE, 1))
    }
}