package io.schiar.mrmine.model

/**
 * Created by Giovani on 21/08/2017.
 */

data class Sequence(val point: Point, val direction: Directions, val quantity: Int) {
    operator fun contains(other: Sequence): Boolean {
        for(point in other.getSequencePoints()) {
            if(point !in getSequencePoints())
                return false
        }

        return true
    }

    override fun toString(): String = "$point, $direction, $quantity"
}

fun Sequence.getSequencePoints() : List<Point?> {
    return this.breakIntoUniqueSequences().map {
        it.point
    }
}

fun Sequence.breakIntoUniqueSequences() : Array<Sequence> {
    return Array<Sequence>(quantity) {
        when(direction) {
            Directions.S -> Sequence(point.goS(it), Directions.UNIQUE, 1)
            Directions.N -> Sequence(point.goN(it), Directions.UNIQUE, 1)
            Directions.E -> Sequence(point.goE(it), Directions.UNIQUE, 1)
            Directions.W -> Sequence(point.goW(it), Directions.UNIQUE, 1)
            Directions.NE -> Sequence(point.goNE(it), Directions.UNIQUE, 1)
            Directions.NW -> Sequence(point.goNW(it), Directions.UNIQUE, 1)
            Directions.SE -> Sequence(point.goSE(it), Directions.UNIQUE, 1)
            Directions.SW -> Sequence(point.goSW(it), Directions.UNIQUE, 1)
            Directions.UNIQUE -> this
        }
    }
}

fun Sequence.nextSequencePoint() : Point = nextSequencePoint(point)

fun Sequence.nextSequencePoint(point: Point) : Point {
    when(direction) {
        Directions.S -> return point.goS(quantity)
        Directions.N -> return point.goN(quantity)
        Directions.E -> return point.goE(quantity)
        Directions.W -> return point.goW(quantity)
        Directions.NE -> return point.goNE(quantity)
        Directions.NW -> return point.goNW(quantity)
        Directions.SE -> return point.goSE(quantity)
        Directions.SW -> return point.goSW(quantity)
        Directions.UNIQUE -> return point
    }
}

fun Sequence.previousSequencePoint() : Point = previousSequencePoint(point)

fun Sequence.previousSequencePoint(point: Point) : Point {
    when(direction) {
        Directions.N -> return point.goS(1)
        Directions.S -> return point.goN(1)
        Directions.E -> return point.goW(1)
        Directions.W -> return point.goE(1)
        Directions.NE -> return point.goSW(1)
        Directions.NW -> return point.goSE(1)
        Directions.SE -> return point.goNW(1)
        Directions.SW -> return point.goNE(1)
        Directions.UNIQUE -> return point
    }
}

fun Sequence.isCompatibleTo(other: Sequence): Boolean {
    when(direction) {
        Directions.N, Directions.S ->
            return other.direction == Directions.N || other.direction == Directions.S
        Directions.E, Directions.W ->
            return other.direction == Directions.E || other.direction == Directions.W
        Directions.NE, Directions.SW ->
            return other.direction == Directions.NE || other.direction == Directions.SW
        Directions.NW, Directions.SE ->
            return other.direction == Directions.NW || other.direction == Directions.SE
        else -> return true
    }
}