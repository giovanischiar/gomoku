package io.schiar.mrmine.model

/**
 * Created by Giovani on 30/08/2017.
 */

class Node(val children : MutableList<Node>, val moves: MutableList<Move>) {
    var score: Long? = null
    var sequences: List<Sequence>? = null
    var oponentsSequences: List<Sequence>? = null
    var board: MutableMap<Point, Char>? = mutableMapOf()
    var degreesOfFreedomTableMe: MutableMap<Point, Int>? = mutableMapOf()
    var degreesOfFreedomTableOponent: MutableMap<Point, Int>? = mutableMapOf()

    fun clear() {
        board = null
        sequences = null
        oponentsSequences = null
        degreesOfFreedomTableMe = null
        degreesOfFreedomTableOponent = null
    }
}