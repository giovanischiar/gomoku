package io.schiar.mrmine.model

/**
 * Created by Giovani on 03/09/17.
 */
class BombCalculator {

    fun bombs(oldTable: MutableMap<Point, Int>, bomb: Point): MutableMap<Point, Int> {
        val table = mutableMapOf<Point, Int>()
        table.putAll(oldTable)

        table.remove(bomb)

        val north = MathUtils.north(bomb.i)
        val south = MathUtils.south(bomb.i)

        val west = MathUtils.west(bomb.j)
        val east = MathUtils.east(bomb.j)

        val southeast = MathUtils.southeast(bomb.i, bomb.j)
        val northwest = MathUtils.northwest(bomb.i, bomb.j)

        val southwest = MathUtils.southwest(bomb.i, bomb.j)
        val northeast = MathUtils.northeast(bomb.i, bomb.j)

        for(i in 0 until south-1) {
            val point = Point(i, bomb.j)
            val value = table[point] ?: throw IllegalStateException("Inconsistent Table")
            table[point] = value - north
        }

        for(i in bomb.i+1 until bomb.i+north) {
            val point = Point(i, bomb.j)
            val value = table[point] ?: throw IllegalStateException("Inconsistent Table")
            table[point] = value - south
        }

        for(j in 0 until east-1) {
            val point = Point(bomb.i, j)
            val value = table[point] ?: throw IllegalStateException("Inconsistent Table")
            table[point] = value - west
        }

        for(j in bomb.j+1 until bomb.j+west) {
            val point = Point(bomb.i, j)
            val value = table[point] ?: throw IllegalStateException("Inconsistent Table")
            table[point] = value - east
        }

        for(k in 1 until northwest) {
            val point = Point(bomb.i+k, bomb.j+k)
            val value = table[point] ?: throw IllegalStateException("Inconsistent Table")
            table[point] = value - southeast
        }

        for(k in 1 until southeast) {
            val point = Point(bomb.i-k, bomb.j-k)
            val value = table[point] ?: throw IllegalStateException("Inconsistent Table")
            table[point] = value - northwest
        }

        for(k in 1 until northeast) {
            val point = Point(bomb.i+k, bomb.j-k)
            val value = table[point] ?: throw IllegalStateException("Inconsistent Table")
            table[point] = value - southwest
        }

        for(k in 1 until southwest) {
            val point = Point(bomb.i-k, bomb.j+k)
            val value = table[point] ?: throw IllegalStateException("Inconsistent Table")
            table[point] = value - northeast
        }

        return table
    }
}