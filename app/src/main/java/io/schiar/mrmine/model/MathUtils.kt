package io.schiar.mrmine.model

import io.schiar.mrmine.R
import io.schiar.mrmine.ResourceProvider

/**
 * Created by Giovani on 03/09/17.
 */
object MathUtils {
    val n = ResourceProvider.int(R.integer.sizeOfGrid)

    fun north(i: Int): Int = (n-1)-i + 1

    fun south(i: Int): Int = i + 1

    fun west(j: Int): Int = (n-1)-j + 1

    fun east(j: Int): Int = j+1

    fun northeast(i: Int, j: Int): Int = Math.min(north(i), east(j))

    fun northwest(i: Int, j: Int): Int = Math.min(north(i), west(j))

    fun southeast(i: Int, j: Int): Int = Math.min(south(i), east(j))

    fun southwest(i: Int, j: Int): Int = Math.min(south(i), west(j))

    fun degreesOfFreedom(i: Int, j: Int): Int {
        return north(i)-1 +
                south(i)-1 +
                west(j)-1 +
                east(j)-1 +
                northeast(i, j)-1 +
                northwest(i, j)-1 +
                southeast(i, j)-1 +
                southwest(i, j)-1
    }
}