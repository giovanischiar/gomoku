package io.schiar.mrmine

/**
 * Created by Giovani on 20/08/2017.
 */
interface BoardView {
    fun runOnMainThread(action: () -> Unit)
    fun reloadBoard()
    fun winner(name: String)
    fun clickButton(rowIndex: Int, columnIndex: Int)
    fun showLoading()
}