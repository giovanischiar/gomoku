package io.schiar.mrmine

import android.widget.Button

/**
 * Created by Giovani on 04/09/2017.
 */
interface StateHolder {
    fun changeCurrentButton(button: Button)
}