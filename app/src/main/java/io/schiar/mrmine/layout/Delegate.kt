package io.schiar.mrmine.layout

import android.view.View

/**
 * Created by Giovani on 19/08/2017.
 */
interface Delegate {
    fun itemClicked(item : View, index: Int)
}