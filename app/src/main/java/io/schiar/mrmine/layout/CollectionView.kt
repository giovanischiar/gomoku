package io.schiar.mrmine.layout

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

/**
 * Created by Giovani on 19/08/2017.
 */

val DEFAULT = -1

class CollectionView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    lateinit var dataSource: DataSource
    var delegate: Delegate? = null

    fun setUp(dataSource: DataSource, delegate: Delegate?) {
        this.dataSource = dataSource
        this.delegate = delegate
        val newParams = this.layoutParams
        if (dataSource.width() != DEFAULT) newParams.width = dataSource.width()
        if (dataSource.height() != DEFAULT) newParams.height = dataSource.height()
        this.layoutParams = newParams
    }

    fun setUp(dataSource: DataSource) {
        setUp(dataSource, null)
    }

    fun applyMargin(item : View) {
        val params = item.layoutParams as ViewGroup.MarginLayoutParams
        if (this.orientation == LinearLayout.VERTICAL) {
            params.topMargin = dataSource.spaceInterItems()
        } else {
            params.leftMargin = dataSource.spaceInterItems()
        }
        item.layoutParams = params
    }

    fun start() {
        for(i in 0..dataSource.amount()-1) {
            val item = dataSource.get(i, this)
            item.setOnClickListener { view ->
                delegate?.itemClicked(item, i)
            }
            if(i != 0) applyMargin(item)
            this.addView(item)
        }
    }

    fun reload() {
        this.removeAllViews()
        start()
    }
}