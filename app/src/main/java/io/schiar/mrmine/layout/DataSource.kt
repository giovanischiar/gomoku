package io.schiar.mrmine.layout

import android.view.View

/**
 * Created by Giovani on 19/08/2017.
 */

interface DataSource {
    fun amount() : Int { return 0 }
    fun spaceInterItems(): Int { return 0 }
    fun height() : Int { return DEFAULT }
    fun width() : Int { return DEFAULT }
    fun get(index: Int, parent: View) : View
}
