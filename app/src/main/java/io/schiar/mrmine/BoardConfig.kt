package io.schiar.mrmine

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.view.View
import android.widget.Button
import io.schiar.mrmine.layout.CollectionView
import io.schiar.mrmine.layout.DataSource

/**
 * Created by Giovani on 19/08/2017.
 */

class BoardConfig(val clickedListener: OnCellClickedListener, val contentConfig: CellContentConfig) : DataSource, StateHolder {
    var currentButton: Button? = null

    override fun changeCurrentButton(button: Button) {
        if(currentButton == null) {
            currentButton = button
        } else {
            paint(currentButton, ResourceProvider.color(R.color.cellColor))
            currentButton = button
        }
        paint(currentButton, ResourceProvider.color(R.color.highlightCellColor))
    }

    fun paint(button: Button?, color: Int) {
        val background = button?.background?.mutate()
        if(background is ShapeDrawable) {
            background.paint.color = color
        } else if(background is GradientDrawable) {
            background.color = ColorStateList.valueOf(color)
        }
        button?.invalidate()
    }


    override fun amount() : Int {
        return ResourceProvider.int(R.integer.sizeOfGrid)
    }

    override fun height() : Int {
        return (ResourceProvider.dimen(R.dimen.cellSize) * ResourceProvider.int(R.integer.sizeOfGrid)).toInt()
    }

    override fun width() : Int {
        return (ResourceProvider.dimen(R.dimen.cellSize) * ResourceProvider.int(R.integer.sizeOfGrid)).toInt()
    }

    override fun get(index: Int, parent: View) : View {
        val item = Inflater.inflateView(R.layout.column, parent)
        val stateHolder = this
        (item as CollectionView).apply {
            val columnConfig = ColumnConfig(index, clickedListener, contentConfig, stateHolder)
            setUp(columnConfig, columnConfig)
            start()
        }
        return item
    }
}