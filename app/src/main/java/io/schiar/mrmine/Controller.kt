package io.schiar.mrmine

import android.util.Log
import io.schiar.mrmine.model.Computer
import io.schiar.mrmine.model.Player

/**
 * Created by Giovani on 19/08/2017.
 */

class Controller(val view: BoardView, val gameParams: GameParams) : OnCellClickedListener, CellContentConfig {
    val players = arrayOf<Player>(Player("Jogador 1", '╳'), Player("Jogador 2", '◯'))
    var miniMax: MiniMax? = null
    lateinit private var currentPlayer : Player

    var board : Array<Array<Char>> = Array(ResourceProvider.int(R.integer.sizeOfGrid),
            { _ -> Array(ResourceProvider.int(R.integer.sizeOfGrid), { _ -> ' '})})


    fun otherPlayer(): Player {
        if(currentPlayer == players[0]) {
            return players[1]
        }
        return players[0]
    }

    fun defineTurn() {
        if(currentPlayer is Computer) {
            view.showLoading()
            Thread(Runnable {
                val miniMax = miniMax ?: throw IllegalArgumentException("Minimax required")
                val point = miniMax.nextTurn(currentPlayer.sequences, otherPlayer().sequences, board)
                view.runOnMainThread {
                    view.clickButton(point.i, point.j)
                }
            }).start()
        }
    }

    fun play() {
        if(gameParams.single) {
            val oldPlayer = players[1]
            players[1] = Computer("Computador", oldPlayer.piece)
            if(gameParams.other) {
                players[0].piece = '◯'
                players[1].piece = '╳'
                currentPlayer = players[1]
                miniMax = MiniMax(players[1].piece, players[0].piece)
            } else {
                currentPlayer = players[0]
                miniMax = MiniMax(players[0].piece, players[1].piece)
            }
        } else {
            currentPlayer = players[0]
        }
        defineTurn()
    }

    override fun click(i: Int, j: Int) {
        currentPlayer.addTurn(i, j)
        Log.d("MrMime", "Sequences of ${currentPlayer.name}: ")
        currentPlayer.sequences.forEach { Log.d("MrMime", it.toString()) }
        board[i][j] = currentPlayer.piece
        //view.reloadBoard()
        if (!searchWinners()) {
            currentPlayer = otherPlayer()
            defineTurn()
        }
        //Toast.makeText(Inflater.context, "clicado em $i, $j", Toast.LENGTH_LONG).show()
    }

    fun searchWinners(): Boolean {
        if(currentPlayer.sequences.none { it.quantity == ResourceProvider.int(R.integer.winnerSequenceQuantity) })
            return false
        view.winner(currentPlayer.name)
        return true
    }

    override fun content(i: Int, j: Int): String {
        return board[i][j].toString()
    }
}
