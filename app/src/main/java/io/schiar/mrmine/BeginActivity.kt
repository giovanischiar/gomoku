package io.schiar.mrmine

import android.annotation.SuppressLint
import android.app.ActionBar
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.TextView

class BeginActivity : Activity() {
    private var gameParams = GameParams()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_begin)
    }

    fun singlePlayer(v: View) {
        gameParams.single = true
        val single = v as Button
        val infoGame = findViewById<TextView>(R.id.infoGame)
        infoGame.text = "Quem começa"
        single.text = "Eu"
        single.setOnClickListener {
            gameParams.me = true
            val intent = Intent(this,  MainActivity::class.java)
            intent.putExtra("gameParams", gameParams)
            startActivity(intent)
        }

        val multi = findViewById<Button>(R.id.multi_button)
        multi.text = "Computador"
        multi.setOnClickListener {
            gameParams.other = true
            val intent = Intent(this,  MainActivity::class.java)
            intent.putExtra("gameParams", gameParams)
            startActivity(intent)
        }
    }

    fun twoPlayers(v: View) {
        val intent = Intent(this,  MainActivity::class.java)
        gameParams.multi = true
        intent.putExtra("gameParams", gameParams)
        startActivity(intent)
    }
}
