package io.schiar.mrmine

import android.graphics.Color
import android.view.View
import android.widget.Button
import io.schiar.mrmine.layout.DataSource
import io.schiar.mrmine.layout.Delegate

/**
 * Created by Giovani on 19/08/2017.
 */
class ColumnConfig(val index: Int, val clickedListener: OnCellClickedListener, val cellContentConfig : CellContentConfig, val stateHolder: StateHolder) : DataSource, Delegate {
    override fun amount() : Int {
        return ResourceProvider.int(R.integer.sizeOfGrid)
    }

    override fun width() : Int {
        return ResourceProvider.dimen(R.dimen.cellSize).toInt()
    }

    override fun height() : Int {
        return (ResourceProvider.dimen(R.dimen.cellSize) * ResourceProvider.int(R.integer.sizeOfGrid)).toInt()
    }

    override fun get(rowIndex: Int, parent: View) : View {
        val item = Inflater.inflateView(R.layout.cell, parent) as Button
        return item
    }

    override fun itemClicked(item: View, rowIndex: Int) {
        clickedListener.click(rowIndex, index)
        val button = item as Button
        button.text = cellContentConfig.content(rowIndex, index)
        if(button.text != " ") {
            button.isEnabled = false
        }
        stateHolder.changeCurrentButton(button)
    }


    fun assignCell(position: Int, value: String) {

    }
}