package io.schiar.mrmine

import java.io.Serializable

/**
 * Created by Giovani on 04/09/17.
 */
data class GameParams(var single: Boolean = false,
                      var multi: Boolean = false,
                      var me: Boolean = false,
                      var other: Boolean = false) : Serializable