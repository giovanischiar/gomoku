package io.schiar.mrmine

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.transition.Visibility
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ProgressBar
import io.schiar.mrmine.layout.CollectionView



class MainActivity : AppCompatActivity(), BoardView {

    lateinit var board: CollectionView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Inflater.init(this)
        ResourceProvider.init(this)
        board = (findViewById(R.id.grid) as CollectionView)
        val intent = getIntent()
        val gameParams = intent.getSerializableExtra("gameParams") as GameParams
        val ctrl = Controller(this, gameParams)
        board.setUp(BoardConfig(ctrl, ctrl))
        board.start()
        ctrl.play()
    }

    override fun showLoading() {
        (findViewById(R.id.loading) as FrameLayout?)?.visibility = View.VISIBLE
    }

    override fun clickButton(rowIndex: Int, columnIndex: Int) {
        (findViewById(R.id.loading) as FrameLayout?)?.visibility = View.GONE
        ((board.getChildAt(columnIndex) as CollectionView?)?.getChildAt(rowIndex) as Button?)?.performClick()
    }

    override fun runOnMainThread(action: () -> Unit) {
        runOnUiThread { action.invoke() }
    }

    override fun reloadBoard() {
        board.reload()
    }

    override fun winner(name: String) {
        val alertDialogBuilder = AlertDialog.Builder(this)

        // set title
        alertDialogBuilder.setTitle("$name venceu!")

        // set dialog message
        alertDialogBuilder
                .setMessage("Clique ok para reiniciar")
                .setCancelable(false)
                .setPositiveButton("ok") { dialog, id ->
                    // if this button is clicked, close
                    // current activity
                    this@MainActivity.recreate()
                }

        // create alert dialog
        val alertDialog = alertDialogBuilder.create()

        // show it
        alertDialog.show()
    }
}
