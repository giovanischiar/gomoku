package io.schiar.mrmine

import android.util.Log
import io.schiar.mrmine.model.*

/**
 * Created by Giovani on 05/09/2017.
 */

class MiniMax(val maxPiece: Char, val minPiece: Char) {
    private val sequencePlanner = SequencePlanner()
    private var heuristicTime: Double = 0.0
    private var heuristicCallCount: Int = 0
    private val degreesOfFreedomTableMe = mutableMapOf<Point, Int>()
    private val degreesOfFreedomTableOponent = mutableMapOf<Point, Int>()
    private val bombCalculator = BombCalculator()
    private var nearMaxDegreesOfFreedom: Int

    init {
        buildDegreesOfFreedomTable(degreesOfFreedomTableMe)
        buildDegreesOfFreedomTable(degreesOfFreedomTableOponent)

        val uniqueMaxDegreesOfFreedom = ResourceProvider.int(R.integer.sizeOfGrid)/2 * 8
        nearMaxDegreesOfFreedom = uniqueMaxDegreesOfFreedom + ((uniqueMaxDegreesOfFreedom-2) * 8)
    }

    private fun buildDegreesOfFreedomTable(table: MutableMap<Point, Int>) {
        for(i in 0 until ResourceProvider.int(R.integer.sizeOfGrid)) {
            for (j in 0 until ResourceProvider.int(R.integer.sizeOfGrid)) {
                table[Point(i, j)] = MathUtils.degreesOfFreedom(i, j)
            }
        }
    }

    fun createTree(maxSequences: List<Sequence>, minSequences: List<Sequence>, board: Array<Array<Char>>, currentMoves: List<Move>): Node {
        val root = Node(mutableListOf<Node>(), mutableListOf())
        root.sequences = maxSequences
        root.oponentsSequences = minSequences
        root.degreesOfFreedomTableMe = degreesOfFreedomTableMe
        root.degreesOfFreedomTableOponent = degreesOfFreedomTableOponent
        for((i, row) in board.withIndex()) {
            for((j, value) in row.withIndex()) {
                if (value != ' ') {
                    root.board?.set(Point(i, j), value)
                }
            }
        }
        generateChildren(root, 0, currentMoves)
        return root
    }

    fun calculateRootMoves(board: Array<Array<Char>>): MutableList<Move> {
        val moves = mutableListOf<Move>()
        for((i, row) in board.withIndex()) {
            for((j, value) in row.withIndex()) {
                if(value != ' ')
                    moves.add(Move(Point(i, j), value))
            }
        }
        return moves
    }

    fun nextTurn(maxSequences: List<Sequence>, minSequences: List<Sequence>, board: Array<Array<Char>>) : Point {
        val currentMoves = calculateRootMoves(board)
        val root = createTree(maxSequences, minSequences, board, currentMoves)
        heuristicTime = 0.0
        heuristicCallCount = 0
        val depthSearchStart = System.currentTimeMillis()
        val bestTurn = miniMax(root, 2, Long.MIN_VALUE, Long.MAX_VALUE)
        val depthSearchTime = System.currentTimeMillis() - depthSearchStart
        Log.d("MrMime", "Next turn calculated. Depth search time: $depthSearchTime. Heuristic function executed $heuristicCallCount times, total time: $heuristicTime.")
        return bestTurn.moves.first().point
    }

    private fun generateChildren(node: Node, level: Int, currentMoves: List<Move>) {
        val n = ResourceProvider.int(R.integer.sizeOfGrid)
        val initialPoint = Point(n / 2, n / 2)
        val treeCreator = TreeCreator(initialPoint, maxPiece, minPiece)
        treeCreator.createTree(node, level, currentMoves)
    }

    fun gameEnding(node: Node): Boolean {
        val move = node.moves.last()
        val newSequence = Sequence(move.point, Directions.UNIQUE, 1)
        val myNewSequences = sequencePlanner.newSequenceListFromBoard(node.sequences, newSequence)
        return (!myNewSequences.none {
            it.quantity == ResourceProvider.int(R.integer.winnerSequenceQuantity)
        })
    }

    fun miniMax(node: Node, levels: Int, alpha: Long, beta: Long): Node {
        var currentAlpha = alpha
        var currentBeta = beta
        if(node.children.isEmpty() || levels == 0) {
            //calcula heuristica
            node.score = heuristic(node)
            return node //folha
        }

        if(levels == 1 && gameEnding(node)) { //to no meu nodo e ganhei
            node.score = heuristic(node)
            return node //folha
        }

        val newLevel = levels - 1
        var bestNode : Node? = null
        for(child in node.children) {
            val move = child.moves.last()
            child.board = mutableMapOf()
            child.board?.putAll(node.board ?: mutableMapOf())
            child.board?.put(move.point, move.piece)
            val newSequence = Sequence(move.point, Directions.UNIQUE, 1)

            if (move.piece == maxPiece) {
                val myNewSequences = sequencePlanner.newSequenceListFromBoard(node.sequences, newSequence)
                child.sequences = myNewSequences
                child.oponentsSequences = node.oponentsSequences
                child.degreesOfFreedomTableMe = node.degreesOfFreedomTableMe
                child.degreesOfFreedomTableOponent = bombCalculator.bombs(node.degreesOfFreedomTableOponent ?: mutableMapOf(), move.point)
            } else {
                val oponentNewSequences = sequencePlanner.newSequenceListFromBoard(node.oponentsSequences, newSequence)
                child.oponentsSequences = oponentNewSequences
                child.sequences = node.sequences
                child.degreesOfFreedomTableMe = bombCalculator.bombs(node.degreesOfFreedomTableMe ?: mutableMapOf(), move.point)
                child.degreesOfFreedomTableOponent = node.degreesOfFreedomTableOponent
            }

            val newChild = miniMax(child, newLevel, currentAlpha, currentBeta)
            val newChildScore = newChild.score ?: throw IllegalArgumentException("Score required")
            if(bestNode == null) {
                bestNode = newChild
                node.score = newChildScore

                if(levels % 2 == 0) { //max
                    Log.d("MrMime", "I found a good move: ${newChild.moves.first().point} with score $newChildScore, assuming player will play at ${newChild.moves.last().point}")
                }
            } else {
                if(levels % 2 == 0) { //max
                    if(bestNode.score!! < newChildScore) {
                        node.score = newChildScore
                        bestNode = newChild
                        Log.d("MrMime", "I found a better move: ${newChild.moves.first().point} with score $newChildScore, assuming player will play at ${newChild.moves.last().point}")
                    }
                } else {
                    if(bestNode.score!! > newChildScore) { //min
                        node.score = newChildScore
                        bestNode = newChild
                    }
                }
            }

            child.clear()
            if(levels % 2 == 0) { //max
                if(newChildScore > currentAlpha){
                    currentAlpha = newChildScore
                }
            } else {
                if(newChildScore < currentBeta){
                    currentBeta = newChildScore
                }
            }
            if (currentAlpha >= currentBeta) {
                break
            }
        }
        return bestNode!!
    }

    fun heuristic(node: Node): Long {
        val startTime = System.nanoTime()
        val evaluated = evaluate(node)
        val endTime = System.nanoTime()
        heuristicTime += (endTime - startTime)/1000000.0
        ++heuristicCallCount
        return evaluated
    }

    fun evaluate(node: Node): Long {
        val board = node.board ?: throw IllegalArgumentException("Board required")
        val mySequencesValue = calcSequenceValues(node.sequences, node.degreesOfFreedomTableMe, board, maxPiece)
        val oponentSequencesValue = calcSequenceValues(node.oponentsSequences, node.degreesOfFreedomTableOponent, board, minPiece)
        return mySequencesValue - oponentSequencesValue
    }

    fun calcSequenceValues(optionalSequences: List<Sequence>?, optionalDegreesOfFreedomTable: MutableMap<Point, Int>?, board: Map<Point, Char>, piece: Char): Long {
        val sequences = optionalSequences ?: throw IllegalArgumentException("Sequences required")
        val degreesOfFreedomTable = optionalDegreesOfFreedomTable ?: throw IllegalArgumentException("Table of degrees of freedom required")

        var total = 0.0
        var total1 = 0.0
        var total2A = 0.0
        var total2 = 0.0
        var total3A = 0.0
        //vitória
        var total3 = 0.0
        var total4 = 0.0
        var total5 = 0.0

        for(sequence in sequences) {
            val quantity = sequence.quantity
            val openings = opening(sequence, board)
            if(quantity != 5 && openings == 0) {
                continue
            }

            val sequenceValue = quantity * quantity * openings

            if(quantity == 1) {
                total1+=sequenceValue
            } else if(quantity == 2 && openings == 1) {
                total2A+=sequenceValue
            } else if(quantity == 2 && openings > 1) {
                total2+=sequenceValue
            } else if(quantity == 3 && openings == 1) {
                total3A+=sequenceValue
            } else if(quantity == 3 && openings > 1) {
                total3+=sequenceValue
            } else if(quantity == 4) {
                total4+=sequenceValue
            } else {
                total5+=sequenceValue
            }
            total += degreesOfFreedom(sequence, degreesOfFreedomTable, board, piece)
        }

        val seq1Value = getCorrectValue({Math.log(total1)}, total1)
        val seq2AValue = getCorrectValue({Math.sqrt(total2A) + nearMaxDegreesOfFreedom}, total2A)
        val seq2Value = getCorrectValue({total2 + nearMaxDegreesOfFreedom}, total2)
        val seq3AValue = getCorrectValue({(total3A * Math.log(total3A)) + nearMaxDegreesOfFreedom}, total3A)
        val seq3Value = getCorrectValue({(total3 * total3) + nearMaxDegreesOfFreedom}, total3)
        val seq4Value = getCorrectValue({Math.pow(2.0, total4) + nearMaxDegreesOfFreedom}, total4)
        val seq5Value = getCorrectValue({Math.pow(3.0, total5) + nearMaxDegreesOfFreedom}, total5)


        total += seq1Value + seq2AValue + seq2Value + seq3AValue + seq3Value + seq4Value + seq5Value
        val result = Math.round(total)
        return result
    }

    fun getCorrectValue(fx: () -> Double, total: Double): Double {
        if(total == 0.0) {
            return total
        }

        return fx()
    }

    fun opening(sequence: Sequence, board: Map<Point, Char>): Int {
        var opens = 0
        val points: MutableList<Point>
        if(sequence.quantity == 1) {
            points = sequence.point.neighbors()
        } else {
            val previous = sequence.previousSequencePoint()
            val next = sequence.nextSequencePoint()
            points = mutableListOf(previous, next)
        }
        for (i in 0 until points.size) {
            if(!board.containsKey(points[i]) && !points[i].outOfGrid())
                opens++
        }
        return opens
    }

    fun degreesOfFreedom(sequence: Sequence, degreesOfFreedomTable: Map<Point, Int>, board: Map<Point, Char>, piece: Char): Int {
        val result: Int
        if(sequence.quantity == 1) {
            result = degreesOfFreedomTable[sequence.point] ?: throw IllegalArgumentException("Table of degrees of freedom should have point ${sequence.point}")
        } else {
            var freedom = 0
            var point = sequence.nextSequencePoint()
            while(board[point] != piece && !point.outOfGrid()) {
                freedom++
                point = sequence.nextSequencePoint(point)
            }

            point = sequence.previousSequencePoint()
            while(board[point] != piece && !point.outOfGrid()) {
                freedom++
                point = sequence.previousSequencePoint(point)
            }
            result = freedom
        }
        return result
    }
}