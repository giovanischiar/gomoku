package io.schiar.mrmine

/**
 * Created by Giovani on 20/08/2017.
 */
interface CellContentConfig {
    fun content(i: Int, j: Int) : String
}