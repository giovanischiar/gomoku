package io.schiar.mrmine

import android.content.Context

/**
 * Created by Giovani on 19/08/2017.
 */
object ResourceProvider {
    lateinit var context: Context

    fun init(context: Context) {
        this.context = context
    }

    fun dimen(code: Int): Float {
        return context.resources.getDimension(code)
    }

    fun int(code: Int): Int {
        return context.resources.getInteger(code)
    }

    fun color(code: Int): Int {
        return context.resources.getColor(code)
    }

}