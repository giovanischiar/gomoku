package io.schiar.mrmine

import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by Giovani on 19/08/2017.
 */
object Inflater {
    lateinit var context: Context

    fun init(context: Context) {
        this.context = context
    }

    fun inflateView(xmlCode: Int, parent: View): View {
        val inflater = context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(xmlCode, parent as ViewGroup, false)
    }
}