package io.schiar.mrmine

import android.view.View

/**
 * Created by Giovani on 19/08/2017.
 */
interface OnCellClickedListener {
    fun click(i: Int, j: Int)
}